@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Provinces</h2>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <a href="{{ route('provinces.create') }}" class="btn btn-primary mb-3">Add Province</a>
    <table class="table">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($provinces as $index => $province)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $province->name }}</td>
                    <td>
                        <a href="{{ route('provinces.edit', $province->id) }}" class="btn btn-warning btn-sm">Edit</a>
                        <form action="{{ route('provinces.destroy', $province->id) }}" method="POST" style="display:inline-block;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
