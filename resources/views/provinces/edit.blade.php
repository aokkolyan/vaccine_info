@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Edit Province</h2>
    <form action="{{ route('provinces.update', $province->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control" value="{{ $province->name }}" required>
        </div>
        <button type="submit" class="btn btn-primary">Update Province</button>
    </form>
</div>
@endsection
