@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Add Province</h2>
    <form action="{{ route('provinces.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control" required>
        </div>
        <button type="submit" class="btn btn-primary">Add Province</button>
    </form>
</div>
@endsection
