<table>
    <thead>
        <tr>
            <th>No</th>
            <th>Province</th>
            <th># of Doses</th>
            <th>Visitor Count</th>
           
        </tr>
    </thead>
    <tbody>
        @foreach($visitors as $index => $visitor)
       
            <tr>
                <td>{{ $index + 1 }}</td>
                <td>{{ $visitor->province->name }}</td>
                <td>{{ $visitor->doses }}</td>
                <td>{{ $visitor->visitor_count }}</td>
                {{-- <td>MOH: {{ $visitor->moh_count }} MOD: {{ $visitor->mod_count }}</td> --}}
            </tr>
        @endforeach
    </tbody>
</table>
