@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Report</h2>
    <table class="table">
        <thead>
            <tr>
                <th>No</th>
                <th>Province</th>
                <th># of Doses</th>
                <th>Visitor Count</th>
                <th>Card Type</th>
            </tr>
        </thead>
        <tbody>
            @foreach($reportData as $index => $data)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $data->province }}</td>
                    <td>{{ $data->doses }}</td>
                    <td>{{ $data->visitor_count }}</td>
                    <td>MOH: {{ $data->moh_count }} MOD: {{ $data->mod_count }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
