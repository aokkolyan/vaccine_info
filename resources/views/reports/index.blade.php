@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Report</h2>
    <form action="{{ route('report.index') }}" method="GET" class="mb-4">
        <div class="form-row">
            <div class="col">
                <select name="province_id" class="form-control">
                    <option value="">Select Province</option>
                    @foreach($provinces as $province)
                        <option value="{{ $province->id }}" {{ request('province_id') == $province->id ? 'selected' : '' }}>
                            {{ $province->name }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="col">
                <input type="number" name="doses" class="form-control" placeholder="Number of Doses" value="{{ request('doses') }}">
            </div>
            <div class="col">
                <input type="text" name="search" class="form-control" placeholder="Search by Province Name" value="{{ request('search') }}">
            </div>
            <div class="col">
                <button type="submit" class="btn btn-primary">Filter</button>
                <a href="{{ route('report.index') }}" class="btn btn-secondary">Clear</a>
            </div>
        </div>
    </form>

    <form action="{{ route('report.export') }}" method="GET" class="mb-4">
        <input type="hidden" name="province_id" value="{{ request('province_id') }}">
        <input type="hidden" name="doses" value="{{ request('doses') }}">
        <input type="hidden" name="search" value="{{ request('search') }}">
        <button type="submit" class="btn btn-success">Export to Excel</button>
    </form>

    <table class="table table-hover" style="cursor: pointer">
        <thead class="table-success">
            <tr>
                <th>No</th>
                <th>Province</th>
                <th># of Doses</th>
                <th>Visitor Count</th>
                <th>Card Type</th>
            </tr>
        </thead>
        <tbody>
            @foreach($visitors as $index => $visitor)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $visitor->province->name}}</td>
                    <td>{{ $visitor->doses }}</td>
                    <td>{{ $visitor->visitor_count }}</td>
                    <td>MOH: {{ $visitor->num_moh }} MOD: {{ $visitor->num_mod }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
