@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Add Visitor</h2>
    <form action="{{ route('visitors.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="province_id">Province</label>
            <select name="province_id" id="province_id" class="form-control" required>
                @foreach($provinces as $province)
                    <option value="{{ $province->id }}">{{ $province->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="doses">Number of Doses</label>
            <input type="number" name="doses" id="doses" class="form-control" required>
        </div>
        <button type="submit" class="btn btn-primary">Add Visitor</button>
    </form>
</div>
@endsection
