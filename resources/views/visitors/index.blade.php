@extends('layouts.app')

@section('content')
<div class="container">
    <h4>List Visitors</h4>
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <a href="{{ route('visitors.create') }}" class=" btn btn-primary mb-3 " style="float: inline-end">Add Visitor</a>
    <table  class="table table-hover" style="cursor: pointer">
        <thead class="table-success">
            <tr>
                <th>No</th>
                <th>Province</th>
                <th>Number of Doses</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($visitors as $index => $visitor)
                <tr>
                    <td>{{ $index + 1 }}</td>
                    <td>{{ $visitor->province->name }}</td>
                    <td>{{ $visitor->doses }}</td>
                    {{-- <td>{{ $visitor->visitor_count  }}</td> --}}
                    <td>
                        <a href="{{ route('visitors.edit', $visitor->id) }}" class="btn btn-warning btn-sm">Edit</a>
                        <form action="{{ route('visitors.destroy', $visitor->id) }}" method="POST" style="display:inline-block;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
