@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Edit Visitor</h2>
    <form action="{{ route('visitors.update', $visitor->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="province_id">Province</label>
            <select name="province_id" id="province_id" class="form-control" required>
                @foreach($provinces as $province)
                    <option value="{{ $province->id }}" {{ $province->id == $visitor->province_id ? 'selected' : '' }}>
                        {{ $province->name }}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="doses">Number of Doses</label>
            <input type="number" name="doses" id="doses" class="form-control" value="{{ $visitor->doses }}" required>
        </div>
        <button type="submit" class="btn btn-primary">Update Visitor</button>
    </form>
</div>
@endsection
