<?php

namespace App\Exports;

use App\Models\Visitor;
use Illuminate\Contracts\View\View;
use App\Enums\VaccineCardTypeEnum;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromView;

class VisitorsExport implements FromView
{
    // public function collection()
    // {
    //     return Visitor::select("id", "province_id", "doses")->get();
    // }
    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function view(): View
    {
        $query = Visitor::query()->with('province');

        if ($this->request->has('province_id') && $this->request->province_id != '') {
            $query->where('province_id', $this->request->province_id);
        }

        if ($this->request->has('doses') && $this->request->doses != '') {
            $query->where('doses', $this->request->doses);
        }

        if ($this->request->has('search') && $this->request->search != '') {
            $query->whereHas('province', function($q) {
                $q->where('name', 'LIKE', '%' . $this->request->search . '%');
            });
        }
        $query = Visitor::with('province')
        ->select(
            'visitors.*',
            DB::raw('COUNT(visitors.id) AS visitor_count'),
            DB::raw('SUM(CASE WHEN visitors.vaccine_card_id = '. VaccineCardTypeEnum::MOH[VaccineCardTypeEnum::getID()] .' THEN 1 ELSE 0 END) AS num_moh'),
            DB::raw('SUM(CASE WHEN visitors.vaccine_card_id = '. VaccineCardTypeEnum::MOD[VaccineCardTypeEnum::getID()] .' THEN 1 ELSE 0 END) AS num_mod'),
        )
        ->groupBy('visitors.province_id');

        $visitors = $query->get();
        // dd($visitors);

        return view('exports.visitors', [
            'visitors' => $visitors
        ]);
     
    }
}
