<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VaccineCard extends Model
{
    use HasFactory;
    protected $table = 'vaccine_cards';
    protected $fillable = ['type'];

}
