<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    use HasFactory;
    protected $table = 'visitors';
    protected $fillable = ['province_id', 'doses']; // Add other fillable fields if needed
   

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function vaccineCards()
    {
        return $this->hasMany(VaccineCard::class);
    }
}
