<?php

namespace App\Http\Controllers;

use App\Models\Province;
use App\Models\VaccineCard;
use App\Models\Visitor;
use Illuminate\Http\Request;

class VisitorController extends Controller
{
    public function index()
    {
        $visitors = Visitor::with('province')->get();
        // dd($visitors);
        return view('visitors.index', compact('visitors'));
    }

    public function create()
    {
        $provinces = Province::all();
        return view('visitors.create', compact('provinces'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'province_id' => 'required|exists:provinces,id',
            'doses' => 'required|integer|min:1',
        ]);

        Visitor::create($request->all());
        return redirect()->route('visitors.index')
                         ->with('success', 'Visitor created successfully.');
    }

    public function edit(Visitor $visitor)
    {
        $provinces = Province::all();
        return view('visitors.edit', compact('visitor', 'provinces'));
    }

    public function update(Request $request, Visitor $visitor)
    {
        $request->validate([
            'province_id' => 'required|exists:provinces,id',
            'doses' => 'required|integer|min:1',
        ]);

        $visitor->update($request->all());
        return redirect()->route('visitors.index')
                         ->with('success', 'Visitor updated successfully.');
    }

    public function destroy(Visitor $visitor)
    {
        $visitor->delete();
        return redirect()->route('visitors.index')
                         ->with('success', 'Visitor deleted successfully.');
    }
}
