<?php

namespace App\Http\Controllers;

use App\Models\Visitor;
use App\Models\Province;
use App\Enums\VaccineCardTypeEnum;
use Illuminate\Http\Request;
use App\Exports\VisitorsExport;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    // public function index(){
    // $reportData = DB::table('visitors')
    //     ->join('provinces', 'visitors.province_id', '=', 'provinces.id')
    //     ->join('vaccine_cards', 'visitors.id', '=', 'vaccine_cards.visitor_id')
    //     ->select('provinces.name as province', DB::raw('count(visitors.id) as visitor_count'), DB::raw('sum(visitors.doses) as doses'), DB::raw('sum(case when vaccine_cards.type = "MOH" then 1 else 0 end) as moh_count'), DB::raw('sum(case when vaccine_cards.type = "MOD" then 1 else 0 end) as mod_count'))
    //     ->groupBy('province')
    //     ->get();

    //     return view('reports.index', compact('reportData'));
    // }
    public function index(Request $request) {

        // SELECT 
        //  vst.*,
        //  prv.name AS province,
        //  COUNT(vst.id) AS visitor_count,
        //  SUM(
        //    CASE WHEN vst.vaccine_card_id = 1 THEN 1 ELSE 0 END
        //  ) AS num_moh,
        //  SUM(
        //    CASE WHEN vst.vaccine_card_id = 2 THEN 1 ELSE 0 END
        //  ) AS num_mod 
        // FROM `visitors` AS vst
        // LEFT JOIN provinces AS prv ON prv.id = vst.province_id
        // GROUP BY vst.province_id

        $query = Visitor::with('province')
                        ->select(
                            'visitors.*',
                            DB::raw('COUNT(visitors.id) AS visitor_count'),
                            DB::raw('SUM(CASE WHEN visitors.vaccine_card_id = '. VaccineCardTypeEnum::MOH[VaccineCardTypeEnum::getID()] .' THEN 1 ELSE 0 END) AS num_moh'),
                            DB::raw('SUM(CASE WHEN visitors.vaccine_card_id = '. VaccineCardTypeEnum::MOD[VaccineCardTypeEnum::getID()] .' THEN 1 ELSE 0 END) AS num_mod'),
                        )
                        ->groupBy('visitors.province_id');

        if ($request->has('province_id') && $request->province_id != '') {
            $query->where('visitors.province_id', $request->province_id);
        }

        if ($request->has('doses') && $request->doses != '') {
            $query->where('visitors.doses', $request->doses);
        }

        if ($request->has('search') && $request->search != '') {
            $query->whereHas('province', function($q) use ($request) {
                $q->where('provinces.name', 'LIKE', '%' . $request->search . '%');
            });
        }
        $visitors = $query->get();

       // return response()->json($visitors);

        $provinces = Province::all();

        // $visitor_count  = DB::table('visitors')
        // ->join('provinces', 'visitors.province_id', '=', 'provinces.id')
        // ->join('vaccine_cards', 'visitors.id', '=', 'vaccine_cards.visitor_id')
        // ->select('provinces.name as province', DB::raw('count(visitors.id) as visitor_count'), DB::raw('sum(visitors.doses) as doses'), DB::raw('sum(case when vaccine_cards.type = "MOH" then 1 else 0 end) as moh_count'), DB::raw('sum(case when vaccine_cards.type = "MOD" then 1 else 0 end) as mod_count'))
        // ->groupBy('province')
        // ->get();

        // return response()->json($visitor_count);

        // dd($count_visitors);
        return view('reports.index', compact('visitors','provinces'));
       
       
    }
    public function export(Request $request)
    {
        return Excel::download(new VisitorsExport($request), 'visitors.xlsx');
        // return Excel::download(new VisitorsExport, 'visitors.xlsx');
        
    }
}
