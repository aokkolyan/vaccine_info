<?php

namespace App\Enums;

class VaccineCardTypeEnum {

    private const ID     = 'id';
    private const TYPE   = 'type';

    const MOH = [ self::ID => 1, self::TYPE => 'MOH'];
    const MOD = [ self::ID => 2, self::TYPE => 'MOD'];

    public static function getID() {
        return self::ID;
    }
}