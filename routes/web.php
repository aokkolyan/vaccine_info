<?php

use App\Http\Controllers\ProvinceController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\VaccineCardController;
use App\Http\Controllers\VisitorController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
   return view('welcome');
});
Route::resource('provinces', ProvinceController::class);
Route::resource('visitors', VisitorController::class);
Route::resource('vaccine-cards', VaccineCardController::class);
Route::get('/report', [ReportController::class, 'index'])->name('report.index');
Route::get('reports/export', [ReportController::class, 'export'])->name('report.export');