<?php

namespace Database\Seeders;

use App\Models\Province;
use App\Models\Visitor;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class VisitorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $phnomPenh = Province::where('name', 'Phnom Penh')->first()->id;
        $kandal = Province::where('name', 'Kandal')->first()->id;
        $pursat = Province::where('name', 'Pursat')->first()->id;

        Visitor::create(['province_id' => $phnomPenh, 'doses' => 5]);
        Visitor::create(['province_id' => $kandal, 'doses' => 1]);
        Visitor::create(['province_id' => $pursat, 'doses' => 3]);
    }
}
