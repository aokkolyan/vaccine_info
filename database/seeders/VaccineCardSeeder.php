<?php

namespace Database\Seeders;

use App\Models\VaccineCard;
use App\Models\Visitor;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class VaccineCardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $visitors = Visitor::all();

        VaccineCard::create(['visitor_id' => $visitors[0]->id, 'type' => 'MOH']);
        VaccineCard::create(['visitor_id' => $visitors[0]->id, 'type' => 'MOD']);
        VaccineCard::create(['visitor_id' => $visitors[1]->id, 'type' => 'MOH']);
        VaccineCard::create(['visitor_id' => $visitors[2]->id, 'type' => 'MOH']);
        VaccineCard::create(['visitor_id' => $visitors[2]->id, 'type' => 'MOD']);
    }
}
