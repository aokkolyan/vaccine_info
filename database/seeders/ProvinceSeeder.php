<?php

namespace Database\Seeders;

use App\Models\Province;
use Illuminate\Database\Seeder;
use NunoMaduro\Collision\Provider;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $provinces = ['Phnom Penh' , 'Kandal' ,'Pursat'];
        foreach ($provinces as $province ) {
            Province::create(['name' => $province]);
        }
;    }
}
